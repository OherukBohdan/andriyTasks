#include "pituh.h"

#include <cstddef>

int main()
{
	const size_t size = 40;
	char *arr = new char[size];
	for (int i=0;i<size-1;i++)
	{
		*(arr+i) = 'a';
	}
	*(arr+size-1) = '\0';
	pituh(arr);
	return 0;
}
